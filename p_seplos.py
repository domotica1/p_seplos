#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on 2022
Publica los datos de seplos
@author: Iqas

CONTRAB:
* * * * * /usr/local/bin/lee_seplos.sh 4201 && USER="mqtt" PWD="mipasswd" DIV="1000" IP="192.168.500.500" /usr/local/bin/p_seplos.py
use DIV="1000" TO divide V into mV, don't set to use V.
"""

import os
import json
from tkinter.tix import INTEGER
import paho.mqtt.client as mqtt #pip3 install paho-mqtt


#GET USER PWD IP FROM OS ENVIRONMENT
USER = os.environ.get('USER') or None
if USER is None:
    raise BaseException('Missing USER env var.')

PWD = os.environ.get('PWD') or None
if PWD is None:
    raise BaseException('Missing PWD env var.')

IP = os.environ.get('IP') or None
if IP is None:
    raise BaseException('Missing IP env var.')

DIV = int( os.environ.get('DIV') ) or None
if DIV is None:
    DIV = int(1)

def max_number (data):  #CALCULATE WICH CELL HAS MAX VOLTAGE
    n_celdas = int (data["NC"])
    largest_number = data["C1"]
    cell_max = [ largest_number , 1 ]

    for i in range(1,n_celdas+1):
        if data["C"+str(i)] > largest_number:
            largest_number = data["C"+str(i)]
            cell_max = [ largest_number , i ]
    
    return (cell_max)

def min_number (data):  #CALCULATE WICH CELL HAS MAX VOLTAGE
    n_celdas = int (data["NC"])
    min_number = data["C1"]
    cell_min = [ min_number , 1 ]

    for i in range(1,n_celdas+1):
        if data["C"+str(i)] < min_number:
            min_number = data["C"+str(i)]
            cell_min = [ min_number , i ]
    
    return (cell_min)


print('-------------- SEPLOS DATA ----------------')

# Opening JSON file
f = open('/tmp/data.json')
data = json.load(f) 
data = data[0]
f.close()

print (data)

cell_max = max_number(data)
cell_min = min_number(data)

client = mqtt.Client("", True, None, mqtt.MQTTv31)
client.username_pw_set(USER,password=PWD)
client.connect(IP,1883,60)


n_celdas = int (data["NC"])
print ("Celdas" + str(n_celdas))
client.publish('bat/Cell_Count',str(data["NC"]))
client.publish('bat/Voltage',str(data["Vtot"]))
client.publish('bat/Cell_MAX_Voltage',str( cell_max[0] / DIV ))
client.publish('bat/Cell_MAX_Cell_Nº',str(cell_max[1]))
client.publish('bat/Cell_MIN_Voltage',str( cell_min[0] / DIV ))
client.publish('bat/Cell_MIN_Cell_Nº',str(cell_min[1]))
client.publish('bat/Diff_MIN_VS_MAX',str( cell_max[0] - cell_min[0] ))

for i in range(1, n_celdas+1):
    client.publish('bat/Cell_'+ str(i) +'_Voltage',str(data[ "C"+str(i) ] / DIV ))

client.publish('bat/Temp_1',str(data["T1"]))
client.publish('bat/Temp_2',str(data["T2"]))
client.publish('bat/Temp_3',str(data["T3"]))
client.publish('bat/Temp_4',str(data["T4"]))
client.publish('bat/SOC',str(data["Soc"]))
client.publish('bat/Cycles',str(data["Cyc"]))
client.publish('bat/Amp',str(data["Amp"]))
client.publish('bat/ResCap',str(data["Rest"]))
client.publish('bat/Cap',str(data["Cap"]))
client.publish('bat/SOH',str(data["SOH"]))
client.publish('bat/PVolt',str(data["PVolt"]))
client.publish('bat/Cycles',str(data["Cyc"]))
client.publish('bat/RatCap',str(data["RC"]))
