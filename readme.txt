Copiamos los ficheros "lee_seplos.sh" y "p_seplos.py" a /usr/local/bin y les damos permisos con chmod 777 nombre de fichero.´

En lee_seplos.sh ponemos el usb-rs485 que tengamos, y en ese no tenéis que tocar nada más.

En la rasp hacéis un crontab (sudo crontab -e) y ponéis la siguiente línea modificando tu usuario, password e ip de tu broker mqtt

* * * * * /usr/local/bin/lee_seplos.sh 4201 && USER="mqtt" PWD="mipasswd" DIV="1000" IP="192.168.500.500" /usr/local/bin/p_seplos.py

Con esto está todo, cada minuto actualiza datos

El fichero sensor.yaml es para integrar los datos por mqtt en Home assistant.

El fichero lovelace.yaml es para el panel de control. Es necesario instalar la tarjeta auto-entities para que funcione la tarjeta con los valores de las celdas ordenadas de mayor a menor voltaje. 

Lo podeis descargar desde https://github.com/thomasloven/lovelace-auto-entities o buscarlo en Hacs.

